#include <Servo.h>
Servo  Servo1;
Servo Servo2;
int dir=0;
int posX=70;
int posY=130;

void setup(){
  Serial.begin(9600);
  Servo1.attach(3,1000,2000);
  Servo1.write(posX);
  Servo2.attach(2,1000,2000);
  Servo2.write(posY);
}

void loop()
{ 
  while(Serial.available() > 0){
    dir=Serial.read();
    Serial.println(dir);
    if(dir >= 49 && dir <= 56){ 
      switch(dir){
       case  49 : 
          posX = max(0,posX-2); 
          break;
       case 50 : 
          posX = min(180,posX+2); 
          break;
       case 51 : 
           posY = min(posY+2,180);
          break;
       case 52 :  
           posY = max(0,posY-2);
            break ;
       case  53 : 
          posX = max(0,posX-10); 
          break;
       case 54 : 
          posX = min(180,posX+10); 
          break;
       case 55 : 
           posY = min(posY+10,180);
          break;
       case 56 :  
           posY = max(0,posY-10);
            break ;
        }
      Servo1.write(posX);
      delay(20);
      Servo2.write(posY);
      delay(20);
      Serial.print("k");
    }
  }
}
