#include <opencv2/opencv.hpp>
#include <iostream>
#include <stdio.h>
#include "serial.h"
#include <unistd.h>
#include <time.h>
using namespace cv;
double centreXinitial=0;
double centreYinitial=0;
 VideoCapture cap(1); 
Mat frame;

void Servomove( double centreX , double centreY,serial_com sp){
char buf[1];
char buf2[1];
int Xecart= centreX-160 ;
int Yecart= centreY-120 ;
printf("%d ||  %F\n",(abs(Xecart)+abs(Yecart))/2,(double)clock());
int i;
buf2[0]='n';
if(centreX != 0 && centreYinitial !=0){
	//mouvement axe X
	if(Xecart <= -25){
			if(Xecart >= -15){
			buf[0]='1';
			serial_write(sp,buf);
			//attente d'une reponse de l'arduino
			while(buf2[0] != 'k'){
				serial_read(sp,buf2);
				}
			}
			else{
			buf[0]='5';
			serial_write(sp,buf);
			//attente d'une reponse de l'arduino
			while(buf2[0] != 'k'){
				serial_read(sp,buf2);
				}
			}		
		}
	else{
	if(Xecart >= 25){
			if(Xecart <= 15){	
			buf[0]='2';
			serial_write(sp,buf);
			//attente d'une reponse de l'arduino
			while(buf2[0] != 'k')
				serial_read(sp,buf2);	
			}
			else{	
			buf[0]='6';
			serial_write(sp,buf);
			//attente d'une reponse de l'arduino
			while(buf2[0] != 'k')
				serial_read(sp,buf2);
			}
		}
		else{	
			}
		}
	buf2[0]='n';
	//mouvement axe Y
	if(Yecart <= -25 ){		
		if(Yecart >=-15){		
		buf[0]='3';
		serial_write(sp,buf);
		printf("send 3\n");
			while(buf2[0] != 'k')
				serial_read(sp,buf2);	
		}else{
			buf[0]='7';
		serial_write(sp,buf);
		printf("send 7\n");
			while(buf2[0] != 'k')
				serial_read(sp,buf2);
		}
	}
	else{
	 if(Yecart >= 25){
		if(Yecart <=15){
		buf[0]='4';
		serial_write(sp,buf);
		printf("send 3\n");
			while(buf2[0] != 'k')
				serial_read(sp,buf2);	
		}else{
			buf[0]='8';
			serial_write(sp,buf);
			printf("send 8\n");
			while(buf2[0] != 'k')
				serial_read(sp,buf2);
		}	
	}
	}
	}
}


int main(int, char**)
{
double color=0;
char open[14] = "/dev/ttyACM0";
serial_com sp=(serial_com)malloc(sizeof(struct serial));
serial_open(sp,open);
double centreX=0;
double centreY=0;
int x=0,y=0,nbPixels=0;
int Sommex,Sommey=0;
  int i,j,count;
  Vec3f pixel;
   // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;
    namedWindow("MyCam",1); 
    cap.set(CV_CAP_PROP_FRAME_WIDTH,320);  //taille de la fenetre
    cap.set(CV_CAP_PROP_FRAME_HEIGHT,240); //au dela de 320*240
MatIterator_<Vec3b> it;
    while(1){
	Sommex=0;
	Sommey=0;
	nbPixels=0;
           if(cap.read(frame)){// get a new frame from camera
		it=frame.begin<Vec3b>();	
			for(y=0;y<240 ; y++){
				for(x=0;x<320 ; x++){
				color=((double)(*it)[2]/(double)((*it)[0]+(*it)[1]+(*it)[2]));
				if(color > 0.90){
					(*it)[0] = 0;
					(*it)[1] = 0;
					(*it)[2] = 255;
					Sommex=Sommex+x;
					Sommey=Sommey+y;
					nbPixels++;		
				}
				else {
					(*it)[0] =0;
					(*it)[1] = 0;
					(*it)[2] = 0;
				}
				++it;		
			}
		}
		//calcul du baricentre
		if(nbPixels > 0){
			centreX=(double)Sommex/(double)nbPixels;
			centreY=(double)Sommey/(double)nbPixels;
		}
		else{
			centreX=0;
			centreY=0;		
		}
		if(centreXinitial!=0 && centreX!=0){
		Servomove(centreX,centreY,sp);
		}
		else{
			centreXinitial=centreX;
			centreYinitial=centreY;
			
		}
		imshow("MyCam", frame);				    
		}
        if(waitKey(30) >= 0) break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}
